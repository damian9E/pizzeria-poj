package main.java.hibernate;
import java.util.List;
import java.util.Date;
import java.util.Iterator;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

/**
 * Created by damianszwichtenberg on 19.06.2016.
 */

public class Hibernate {

    private static SessionFactory factory;
    static SessionFactory getFactory() {
        try {
            factory = new Configuration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("Failed to create sessionFactory object." + ex);
            throw new ExceptionInInitializerError(ex);
        }
        return factory;
    }

    /* Method to CREATE an Pizza in the database */
    public Integer addPizza(String nazwa, String składniki, int ostrość, double cenaMała, double cenaŚrednia, double cenaDuża){
        Session session = factory.openSession();
        Transaction tx = null;
        Integer pizzaID = null;
        try{
            tx = session.beginTransaction();
            Pizza pizza = new Pizza(nazwa, składniki, ostrość, cenaMała, cenaŚrednia, cenaDuża);
            pizzaID = (Integer) session.save(pizza);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
        return pizzaID;
    }
    /* Method to DELETE an Pizza from the records */
    public void deletePizza(Integer pizzaID) {
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Pizza pizza =
                    (Pizza) session.get(Pizza.class, pizzaID);
            session.delete(pizza);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }
    /* Method to CREATE an Zamówienie in the database */
    public Integer addZamówienie(int idPizza, int idPizzeria, String adresDostawy, double doZapłaty){
        Session session = factory.openSession();
        Transaction tx = null;
        Integer zamówienieID = null;
        try{
            tx = session.beginTransaction();
            Zamówienie zamówienie = new Zamówienie(idPizza, idPizzeria, adresDostawy, doZapłaty);
            zamówienieID = (Integer) session.save(zamówienie);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
        return zamówienieID;
    }
    /* Method to DELETE an Zamówienie from the records */
    public void deleteZamówienie(Integer zamówienieID){
        Session session = factory.openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            Zamówienie zamówienie =
                    (Zamówienie) session.get(Zamówienie.class, zamówienieID);
            session.delete(zamówienie);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }
    /* Method to CREATE an Pizzera in the database */
    public Integer addPizzeria(String adres){
        Session session = factory.openSession();
        Transaction tx = null;
        Integer pizzeriaID = null;
        try{
            tx = session.beginTransaction();
            Pizzeria pizzeria = new Pizzeria(adres);
            pizzeriaID = (Integer) session.save(pizzeria);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
        return pizzeriaID;
    }
    /* Method to DELETE an Pizzeria from the records */
    public void deletePizzeria(Integer pizzeriaID){
        Session session = factory.openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            Pizzeria pizzeria =
                    (Pizzeria) session.get(Pizzeria.class, pizzeriaID);
            session.delete(pizzeria);
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }
    /* Method to  READ all the Pizzas */
    public void listPizzas( ){
        Session session = factory.openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            List pizzas = session.createQuery("FROM Pizza").list();
            for (Iterator iterator =
                 pizzas.iterator(); iterator.hasNext();){
                Pizza pizza = (Pizza) iterator.next();
                //System.out.print("First Name: " + employee.getFirstName());
                //System.out.print("  Last Name: " + employee.getLastName());
                //System.out.println("  Salary: " + employee.getSalary());
            }
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }
    /* Method to  READ all the Zamówienia */
    public void listZamówienia( ){
        Session session = factory.openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            List zamówienia = session.createQuery("FROM Zamówienie").list();
            for (Iterator iterator =
                 zamówienia.iterator(); iterator.hasNext();){
                Zamówienie zamówienie = (Zamówienie) iterator.next();
                //System.out.print("First Name: " + employee.getFirstName());
                //System.out.print("  Last Name: " + employee.getLastName());
                //System.out.println("  Salary: " + employee.getSalary());
            }
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }
    /* Method to  READ all the pizzerias */
    public void listPizzerias( ){
        Session session = factory.openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            List pizzerias = session.createQuery("FROM Pizzeria ").list();
            for (Iterator iterator =
                 pizzerias.iterator(); iterator.hasNext();){
                Pizzeria pizzeria = (Pizzeria) iterator.next();
                //System.out.print("First Name: " + employee.getFirstName());
                //System.out.print("  Last Name: " + employee.getLastName());
                //System.out.println("  Salary: " + employee.getSalary());
            }
            tx.commit();
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            session.close();
        }
    }

}