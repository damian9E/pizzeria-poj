package main.java.hibernate;
import javax.persistence.*;
/**
 * Created by damianszwichtenberg on 19.06.2016.
 */
@Entity
@Table(name = "PIZZERIA")
public class Zamówienie {

    @Id @GeneratedValue
    @Column(name = "ID")
    private int id;

    @Column(name = "IDPIZZA")
    private int idPizza;

    @Column(name = "IDPIZZERIA")
    private int idPizzeria;

    @Column(name = "ADRESDOSTAWY")
    private String adresDostawy;

    @Column(name = "DOZAPŁATY")
    private double doZapłaty;

    public Zamówienie() {}
    public Zamówienie(int idPizza, int idPizzeria, String adresDostawy, double doZapłaty)   {
        this.idPizza = idPizza;
        this.idPizzeria = idPizzeria;
        this.adresDostawy = adresDostawy;
        this.doZapłaty = doZapłaty;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdPizza() {
        return idPizza;
    }

    public void setIdPizza(int idPizza) {
        this.idPizza = idPizza;
    }

    public int getIdPizzeria() {
        return idPizzeria;
    }

    public void setIdPizzeria(int idPizzeria) {
        this.idPizzeria = idPizzeria;
    }

    public String getAdresDostawy() {
        return adresDostawy;
    }

    public void setAdresDostawy(String adresDostawy) {
        this.adresDostawy = adresDostawy;
    }

    public double getDoZapłaty() {
        return doZapłaty;
    }

    public void setDoZapłaty(double doZapłaty) {
        this.doZapłaty = doZapłaty;
    }
}
