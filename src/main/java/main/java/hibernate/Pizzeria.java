package main.java.hibernate;
import javax.persistence.*;
/**
 * Created by damianszwichtenberg on 19.06.2016.
 */
@Entity
@Table(name = "PIZZERIA")
public class Pizzeria {

    @Id @GeneratedValue
    @Column(name = "ID")
    private int id;

    @Column(name = "ADRES")
    private String adres;

    public Pizzeria()   {}
    public Pizzeria(String adres)   {
        this.adres = adres;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAdres() {
        return adres;
    }

    public void setAdres(String adres) {
        this.adres = adres;
    }
}
