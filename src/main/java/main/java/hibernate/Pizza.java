package main.java.hibernate;
import javax.persistence.*;
/**
 * Created by damianszwichtenberg on 19.06.2016.
 */
@Entity
@Table(name = "PIZZA")
public class Pizza {

    @Id @GeneratedValue
    @Column(name = "ID")
    private int id;

    @Column(name = "NAZWA")
    private String nazwa;

    @Column(name = "SKŁADNIKI")
    private String składniki;

    @Column(name = "OSTROŚĆ")
    private int ostrość;

    @Column(name = "CENAMAŁA")
    private double cenaMała;

    @Column(name = "CENAŚREDNIA")
    private double cenaŚrednia;

    @Column(name = "CENADUŻA")
    private double cenaDuża;

    public Pizza()  {}
    public Pizza(String nazwa, String składniki, int ostrość, double cenaMała, double cenaŚrednia, double cenaDuża)  {
        this.nazwa = nazwa;
        this.składniki = składniki;
        this.ostrość = ostrość;
        this.cenaMała = cenaMała;
        this.cenaŚrednia = cenaŚrednia;
        this.cenaDuża = cenaDuża;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getSkładniki() {
        return składniki;
    }

    public void setSkładniki(String składniki) {
        this.składniki = składniki;
    }

    public int getOstrość() {
        return ostrość;
    }

    public void setOstrość(int ostrość) {
        this.ostrość = ostrość;
    }

    public double getCenaMała() {
        return cenaMała;
    }

    public void setCenaMała(double cenaMałaPizza) {
        this.cenaMała = cenaMałaPizza;
    }

    public double getCenaŚrednia() {
        return cenaŚrednia;
    }

    public void setCenaŚrednia(double cenaŚredniaPizza) {
        this.cenaŚrednia = cenaŚredniaPizza;
    }

    public double getCenaDuża() {
        return cenaDuża;
    }

    public void setCenaDuża(double cenaDużaPizza) {
        this.cenaDuża = cenaDużaPizza;
    }
}
